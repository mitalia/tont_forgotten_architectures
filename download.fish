#!/usr/bin/env fish
test -d download; or mkdir download
cd download
for i in (cat ../list.txt)
    wget $i -O (echo $i | cut -d "/" -f 5)".html"
end
