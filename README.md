# Downloader for The Old New Thing - Forgotten Architectures Series #

Downloads and puts together in a single HTML file (suitable for easy eBook conversion) all the "strange architectures" articles of Raymond Chen's The Old New Thing blog.

Can be easily used for other TONT compilations - just tweak `list.txt` and possibly `process.py` (to change the header).

1. Download the stuff (`./download.fish`);
2. Process the stuff (`./process.py download/*.html > out.html`).

For ease of use, the current output is already committed.
