#!/usr/bin/env python2
from bs4 import BeautifulSoup
import re
import os.path
import sys

print """<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>The Old New Thing - The Lost Architectures Guide</title>
</head>
<body>"""

files = list(sorted(sys.argv[1:]))
secs = [os.path.splitext(os.path.basename(f))[0] for f in files]
for f,sec in zip(files, secs):
    with open(f, "rb") as ff:
        s = BeautifulSoup(ff, "lxml")
        title = s.find('h1', class_='entry-title')
        title["id"] = sec
        print title
        article = s.find(class_='entry-content single')
        for a in article.find_all('a', href = True):
            m = re.match(r'https://blogs.msdn.microsoft.com/oldnewthing/([^/]*)/\?p=', a['href'])
            if m:
                if m.group(1) in secs:
                    a['href']='#'+m.group(1)
        print article
        print "<hr>"
print "</body>"
print "</html>"
